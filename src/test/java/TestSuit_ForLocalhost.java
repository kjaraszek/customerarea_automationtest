import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Created by kjaraszek on 2/20/2018.
 */
public class TestSuit_ForLocalhost extends Initializer {

// Old test suit, now we will use TestSuit with domain parameter


    @Test
    @Parameters
    public void logToCustomerAreaonLocahost() {     // TEST OK
        Dashboard dashboard = new Dashboard(driver);
        dashboard.openLocalHost();
        dashboard.loginToCustomerArea();
    }
    @Test
    public void dashboard_ClickinSeeAllonSection_ItRedirectToOtherPage() { // TEST OK
        MyDocuments myDocuments = new MyDocuments(driver);
        myDocuments.openLocalHost();
        myDocuments.loginToCustomerArea();
        myDocuments.checkIsSeeAllRedirectToCorrectSiteFirstSection();
    }
    @Test
    public void loginView_ChangeLanguage_LanguageIsChangedOnFrench () {  // TEST OK
        Dashboard dashboard = new Dashboard(driver);
        dashboard.openLocalHost();
        dashboard.setLanguageOnFrenchandCheckIsChanged();
    }

    @Test
    public void trends_ClickonDate_DatepicekerIsOpen () {
        Reports charts = new Reports(driver);
        charts.openLocalHost();
        charts.loginToCustomerArea();
        charts.opendatepickerFrom();
        charts.checkisDatepickerOpen();

    }
    @Test
    public void trends_CheckIsAllCharsDisplaying_ChartsAreDisplaying () {
        Reports charts = new Reports(driver);
        charts.openLocalHost();
        charts.loginToCustomerArea();
        charts.checkIsChartsDisplayingOnLocalhost();
    }
    @Test
    public void trends_ImpersonateAsTBSCGCheckIsAllCharsDisplaying_ChartsAreDisplaying () {
        Reports charts = new Reports(driver);
        charts.openLocalHost();
        charts.loginToCustomerArea();
        charts.impersonateAsTBSCG();
        charts.checkIsChartsDisplayingOnLocalhost();
    }

    // Utworzone po 23.01.2018  ( nie dodane do Ebadu)

    @Test
    public void dashboard_ContactMebuttonClick_MessageIsSent () {
        Dashboard dashboard = new Dashboard(driver);
        dashboard.openLocalHost();
        dashboard.loginToCustomerArea();
        dashboard.checkIsMyOrderContactMeSend();


    }
    @Test
    public void dashboard_LogInAndLogOut_UserIsLoggedOut() {
        Dashboard dashboard = new Dashboard(driver);
        dashboard.openLocalHost();
        dashboard.loginToCustomerArea();
        dashboard.logOut();
        dashboard.checkIsUserLoggedOut();

    }
    @Test
    public void dashboard_IncorrectLogin_CheckIncorrectLoginStatement(){
        Dashboard dashboard = new Dashboard(driver);
        dashboard.openLocalHost();
        dashboard.incorrectLogin();
        dashboard.checkIsIncorrectPasswqordstatement();
    }

    @Test
    @Parameters({"whatPageType"})
    public void checkingStuff(String whatPageType){
        Dashboard dashboard = new Dashboard(driver);
        driver.get(dashboard.getPageType(whatPageType)+dashboard);

    }




}
