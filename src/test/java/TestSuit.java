import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.unitils.reflectionassert.ReflectionAssert;



/**
 * Created by kjaraszek on 12/14/2017.
 */
public class TestSuit extends Initializer {

    @Test
    @Parameters({"whatPageType"})
    public void logToCustomerArea(String whatPageType) {     // TEST OK
        Dashboard dashboard = new Dashboard(driver);
        driver.get(dashboard.getPageType(whatPageType));
        dashboard.closeCoockie();
        dashboard.loginToCustomerArea();
    }

    @Test
    @Parameters({"whatPageType"})
    public void dashboard_ClickinSeeAllonSection_ItRedirectToOtherPage(String whatPageType) { // TEST OK
        MyDocuments myDocuments = new MyDocuments(driver);
        driver.get(myDocuments.getPageType(whatPageType));
        myDocuments.closeCoockie();
        myDocuments.loginToCustomerArea();
        myDocuments.checkIsSeeAllRedirectToCorrectSiteFirstSection();
    }
    @Test
    @Parameters({"whatPageType"})
    public void loginView_ChangeLanguage_LanguageIsChangedOnFrench (String whatPageType) {  // TEST OK
        Dashboard dashboard = new Dashboard(driver);
        driver.get(dashboard.getPageType(whatPageType));
        dashboard.closeCoockie();
        dashboard.setLanguageOnFrenchandCheckIsChanged();
    }

    @Test
    @Parameters({"whatPageType"})
    public void trends_ClickonDate_DatepicekerIsOpen (String whatPageType) {
        Reports charts = new Reports(driver);
        driver.get(charts.getPageType(whatPageType));
        charts.closeCoockie();
        charts.loginToCustomerArea();
        charts.opendatepickerFrom();
        charts.checkisDatepickerOpen();

    }
    @Test
    @Parameters({"whatPageType"})
    public void trends_CheckIsAllCharsDisplaying_ChartsAreDisplaying (String whatPageType) {
        Reports charts = new Reports(driver);
        driver.get(charts.getPageType(whatPageType));
        charts.closeCoockie();
        charts.loginToCustomerArea();
        charts.checkIsChartsDisplaying();
    }
    @Test
    @Parameters({"whatPageType"})
    public void trends_ImpersonateAsTBSCGCheckIsAllCharsDisplaying_ChartsAreDisplaying (String whatPageType) {
        Reports charts = new Reports(driver);
        driver.get(charts.getPageType(whatPageType));
        charts.closeCoockie();
        charts.loginToCustomerArea();
        charts.impersonateAsTBSCG();
        charts.checkIsChartsDisplaying();
    }

    // Utworzone po 23.01.2018  ( nie dodane do Ebadu)

    @Test
    @Parameters({"whatPageType"})
    public void dashboard_ContactMebuttonClick_MessageIsSent (String whatPageType) {
        Dashboard dashboard = new Dashboard(driver);
        driver.get(dashboard.getPageType(whatPageType));
        dashboard.closeCoockie();
        dashboard.loginToCustomerArea();
        dashboard.checkIsMyOrderContactMeSend();
    }
    @Test
    @Parameters({"whatPageType"})
    public void dashboard_IncorrectLogin_CheckIncorrectLoginStatement(String whatPageType){
        Dashboard dashboard = new Dashboard(driver);
        driver.get(dashboard.getPageType(whatPageType));
        dashboard.closeCoockie();
        dashboard.incorrectLogin();
        dashboard.checkIsIncorrectPasswqordstatement();
    }

    @Test
    @Parameters({"whatPageType"})
    public void dashboard_CloseLeftMenu_LeftMenuIsClosed(String whatPageType){
        Dashboard dashboard = new Dashboard(driver);
        driver.get(dashboard.getPageType(whatPageType));
        dashboard.closeCoockie();
        dashboard.loginToCustomerArea();
        dashboard.closeOrOpenLeftMenu();
        dashboard.checkIslefMenuClosed();

    }
    @Test
    @Parameters({"whatPageType"})
    public void myInvoices_ClickUnClickColumnName_CkechIsColumnNameCorrect(String whatPageType){
        MyInvoices myInvoices = new MyInvoices(driver);
        myInvoices.closeCoockie();
        myInvoices.loginToCustomerArea();
        myInvoices.openMyInvoices();

    }

    @Test
    @Parameters({"whatPageType"})
    public void myDocuments_ClickUnClickColumnName_CheckIsColumnNameCorrect(String whatPageType){
        MyDocuments myDocuments = new MyDocuments(driver);
        driver.get(myDocuments.getPageType(whatPageType));
        myDocuments.closeCoockie();
        myDocuments.loginToCustomerArea();
        myDocuments.openMyDocuments();
        myDocuments.pause(2);
        myDocuments.openSelectColumn();
        myDocuments.clickAllColumnsCheckBox();
        myDocuments.checkTopColumnListElementsNumber();
        myDocuments.clickAllColumnsCheckBox();
        ColumnAndCheckBoxCompare namesOnTopColumn = new ColumnAndCheckBoxCompare("","","",
                "","");
        ColumnAndCheckBoxCompare namesOnCheckboxlist = new ColumnAndCheckBoxCompare("","","",
                "","");
        namesOnTopColumn.size = myDocuments.takeNameFromColumnTop(MyDocuments.ColumntTopName.SIZE);
        namesOnTopColumn.createdOn = myDocuments.takeNameFromColumnTop(MyDocuments.ColumntTopName.CREATEDON);
        namesOnTopColumn.createdBy = myDocuments.takeNameFromColumnTop(MyDocuments.ColumntTopName.CREATEDBY);
        namesOnTopColumn.edit = myDocuments.takeNameFromColumnTop(MyDocuments.ColumntTopName.EDIT);
        namesOnTopColumn.download = myDocuments.takeNameFromColumnTop(MyDocuments.ColumntTopName.DOWNLOAD);
        namesOnCheckboxlist.size = myDocuments.takenNameFromCheckBoxList(MyDocuments.ColumntTopName.SIZE);
        namesOnCheckboxlist.createdOn = myDocuments.takenNameFromCheckBoxList(MyDocuments.ColumntTopName.CREATEDON);
        namesOnCheckboxlist.createdBy = myDocuments.takenNameFromCheckBoxList(MyDocuments.ColumntTopName.CREATEDBY);
        namesOnCheckboxlist.edit = myDocuments.takenNameFromCheckBoxList(MyDocuments.ColumntTopName.EDIT);
        namesOnCheckboxlist.download = myDocuments.takenNameFromCheckBoxList(MyDocuments.ColumntTopName.DOWNLOAD);
        ReflectionAssert.assertReflectionEquals(namesOnCheckboxlist,namesOnTopColumn);
    }
    private class ColumnAndCheckBoxCompare {
        private String size;
        private String createdOn;
        private String createdBy;
        private String edit;
        private String download;

        private ColumnAndCheckBoxCompare (  String size,
                 String createdOn,
                String createdBy,
                 String edit,
                 String download){
            this.size = size;
            this.createdOn = createdOn;
            this.createdBy = createdBy;
            this.edit = edit;
            this.download = download;}
        }
}



