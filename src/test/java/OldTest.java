
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class OldTest extends Initializer {
    @Test
    public void dashboard_clickCloseSectionbutton_SectionIsClosed() {  // TEST OK - test do wywalenia, nie będzie zamykania kafelków
        Dashboard dashboard = new Dashboard(driver);
        dashboard.openHomePage();
        dashboard.closeCoockie();
        dashboard.loginToCustomerArea();
        dashboard.closeFirstSection();
        dashboard.checkIsSectionClosed(); }

    @Test
    public void dashboard_MoveSectionToAnother_SectionIsMoved() { // Przenoszenie kafelek ciągle nie działa
    Dashboard dashboard = new Dashboard(driver);
        dashboard.openHomePage();
        dashboard.closeCoockie();
        dashboard.loginToCustomerArea();
        dashboard.moveFirstSectionToSecondSection();}
}