import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.io.IOException;

/**
 * Created by kjaraszek on 1/26/2018.
 */
public class Initializer {
    public static WebDriver driver=null;

    @BeforeMethod
    public static void  createDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\src\\drivers\\chromedriver_2.34.exe");
        driver = new ChromeDriver();
    }
    @AfterClass
    @Parameters({"close-browser"})
    public void closeBrowser(Boolean closeBrowser) throws IOException {
        if (closeBrowser) {
            try {
                if (driver != null) {
                    driver.quit();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage() + " - unable to close browser");
            }
        }


    }
}