import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

/**
 * Created by kjaraszek on 12/21/2017.
 */
public class MyDocuments extends Dashboard {
    public MyDocuments(WebDriver driver) {
        super(driver);
    }

    // xpaths
    @FindBy (how = How.XPATH,using ="//h1" )
    private WebElement myDocumentsTitle;

    @FindBy (how = How.XPATH,using = "(//span[@class='pr-2'])[1]")
    private WebElement seeAllFromFirstSections;

    @FindBy (how = How.XPATH,using = "(//div[@class='card-header'])[1]//span")
    private WebElement titleFromFirstSection;

    @FindBy (how = How.XPATH,using = "//input[@placeholder='Search']")
    private WebElement searchInputSpace;

    @FindBy (how = How.XPATH,using = "//button[@class='btn btn-info dropdown-toggle']")
    private WebElement selectColumnsButton;

    @FindAll({@FindBy(xpath="//th")})
    private List<WebElement> topColumnsList;

    @FindAll({@FindBy(xpath="//label[@class='d-block my-2 mx-3 custom-control custom-checkbox']")})
    private List<WebElement> selectColumnCheckBoxList;








    //strings

    //Methods

    public void checkIsSeeAllRedirectToCorrectSiteFirstSection() {
        waitForClickableWebElement(seeAllFromFirstSections);
        sectiontitleOnDashboard = titleFromFirstSection.getText();
        seeAllFromFirstSections.click();
        Assert.assertEquals(sectiontitleOnDashboard, myDocumentsTitle.getText());
    }
    public void openMyDocuments() {
        myDocumentsButtonOnMenu.click();
        pause(5);
    }
    public void openSelectColumn (){
        selectColumnsButton.click();
    }
    public void clickAllColumnsCheckBox() {
        for ( int i=0; i<5; i++){
            selectColumnCheckBoxList.get(i).click();
            }
        }
    public void checkTopColumnListElementsNumber (){
        Assert.assertEquals(topColumnsList.size(),1);
    }
    // Created after 28.02.2018

    public String takeNameFromColumnTop(ColumntTopName columnTopName){
        switch (columnTopName){
            case SIZE:
                return topColumnsList.get(1).getText();
            case CREATEDON:
                return topColumnsList.get(2).getText();
            case CREATEDBY:
                return topColumnsList.get(3).getText();
            case EDIT:
                return topColumnsList.get(4).getText();
            case DOWNLOAD:
                return topColumnsList.get(5).getText();
        }
        return null;
    }

    public enum ColumntTopName {
        SIZE,CREATEDON,CREATEDBY,EDIT,DOWNLOAD
    }
    public String takenNameFromCheckBoxList (ColumntTopName columntTopName){
        switch (columntTopName){
            case SIZE:
                return selectColumnCheckBoxList.get(0).getText();
            case CREATEDON:
                return selectColumnCheckBoxList.get(1).getText();
            case CREATEDBY:
                return  selectColumnCheckBoxList.get(2).getText();
            case EDIT:
                return selectColumnCheckBoxList.get(3).getText();
            case DOWNLOAD:
                return  selectColumnCheckBoxList.get(4).getText();
        }
        return null;
    }


}

