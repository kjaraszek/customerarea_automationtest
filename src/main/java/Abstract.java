import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by kjaraszek on 12/14/2017.
 */
public abstract class Abstract {
    //Strings and Integers
    String homepage = "http://192.168.178.207:8083/customer-area/#/en/pages/login";
    String magnolialogin = "superuser";
    String customerAreaLogin = "ebltstest";
    String customerAreaPassword = "jewe3Ecrustu";
    String pasttitle;
    String presenttitle;
    String sectiontitleOnDashboard;
    String titleOnSeeAllPage;
    String sendMessage = "Sent";
    String localhost = "http://localhost:8081/#/en/pages/login";
    String sendMessageforLocalhost = "Not sent";

    @FindBy(how = How.XPATH,using = "//button[@class='navbar-toggler sidebar-toggler d-md-down-none']")
    WebElement leftMenuOpenCloseButton;

    @FindBy (how = How.XPATH,using = "//div[@class='app magnolia sidebar-minimized']")
    WebElement leftMenuMinimalized;


    protected WebDriver driver;

    public Abstract (WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(this.driver, this);
    }


    public void waitForClickableWebElement (final WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }
    public void waitFormVisibleWebElement ( final WebElement webElement){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }
    public void pause(Integer seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        }
        catch (InterruptedException e){

        }
    }
    // Added after 22.02
    public void closeOrOpenLeftMenu(){
        leftMenuOpenCloseButton.click();
        pause(1);

    }
    public void checkIslefMenuClosed() {
        Assert.assertTrue(leftMenuMinimalized.isEnabled());


    }


    public String pageType;

    public String getPageType(String pageParameter){
        if (pageParameter.equals("openPageViaIP")){
            pageType= homepage; }
        else if (pageParameter.equals("openLocalHost")){
            pageType= localhost;
        }

        return pageType;
    }
}
