import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.ClickAndHoldAction;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by kjaraszek on 12/14/2017.
 */
public class Dashboard extends Abstract {
    public Dashboard(WebDriver driver) {
        super(driver);
    }




    //WebElements
    @FindBy(how = How.XPATH,using ="//input[@id='login-username']" )
    private WebElement userNameInputSpaceMagnolia;

    @FindBy(how = How.XPATH,using ="//input[@id='login-password']" )
    private WebElement passwordInputSpaceMagnolia;

    @FindBy(how = How.XPATH,using = "//button[@id='login-button']")
    private WebElement loginButtonMagnolia;

    @FindBy (how = How.XPATH,using = "//input[@name='username']")
    private WebElement usernameInputSpaceCustomerArea;

    @FindBy (how = How.XPATH,using = "//input[@name='password']")
    private WebElement passwordInputSpaceCustomerArea;

    @FindBy (how = How.XPATH,using = "//button[@class='btn btn-primary']")
    private WebElement loginButtonCustomerArea;

    @FindBy (how = How.XPATH,using = "//i[@class='icon-cancel-circle']")
    private WebElement closeCoockieButton;

    @FindBy (how = How.XPATH,using = "(//*[@class='drag btn btn-transparent'])[1]")
    private WebElement buttonForMoveFirstSection;

    @FindBy (how = How.XPATH,using = "(//*[@class='drag btn btn-transparent'])[2]")
    private WebElement buttonForMoveSecondSection;


    @FindBy (how = How.XPATH,using = "(//div[@class='card-header'])[2]")
    private WebElement titleFromSecondSection;


    @FindBy (how = How.XPATH,using = "(//button[@class='pull-right close-icon btn btn-transparent'])[2]")
    private WebElement closeSecondSectionButton;

    @FindBy (how = How.XPATH,using = "(//button[@class='pull-right close-icon btn btn-transparent'])[1]")
    private WebElement closeFirstSectionButton;

    @FindBy (how = How.XPATH,using = "(//div[@class='card-header'])[1]")
    private WebElement titleFromFirstSection;

    @FindBy ( how = How.XPATH, using = "(//span[@class='pr-1'])[2]")
    private WebElement seeAllFromSecondSection;

    @FindBy (how = How.XPATH,using = "//button[@class='btn px-0 btn-link']")
    private WebElement changeLanguageButtonOnLoginpage;

    @FindBy (how = How.XPATH,using = "//button[text()[contains(.,'English')]]")
    private WebElement englishLanguageOnPopupLoginpage;

    @FindBy (how = How.XPATH,using = "//button[text()[contains(.,'Français')]]")
    private WebElement frenchLanguageOnPopupLoginPage;

    @FindBy (how = How.XPATH,using = "//button[@class='btn btn-link btn-block disabled' and text()[contains(.,'Français')]]")
    private  WebElement frenchLanguageDisabled;

    @FindBy (how = How.XPATH,using = "//button[@class='btn btn-link btn-block disabled' and text()[contains(.,'English')]]")
    private  WebElement englishLanguageDisabled;

    @FindBy (how = How.XPATH,using = "//h1[text()='Login']")
    private WebElement englishLanguageOnLoginpage;

    @FindBy (how = How.XPATH,using = "//a[@href='#/en/charts']")
    WebElement chartsButtonOnMenu;

    @FindBy (how = How.XPATH,using = "//span[contains(text(), 'Impersonate')]")
    WebElement impersonateDropdown;

    @FindBy (how = How.XPATH,using = "//a[@class='dropdown-item' and contains(text(), 'TBSCG')]")
    WebElement tbscgImpersonateDropdown;

    @FindBy (how = How.XPATH,using = "//a[@class='dropdown-item' and contains(text(), 'HERBALIFE')]")
    WebElement herbalifeImpersonateDropdown;

    @FindBy (how = How.XPATH,using = "//a[@href='#/en/documents']")
    WebElement myDocumentsButtonOnMenu;

    @FindBy (how = How.XPATH,using = "//button[contains(text(), 'Contact')]")
    private WebElement contactMeButton;

    @FindBy (how = How.XPATH,using = "//div[@class='warning mb-1']")
    private WebElement statementAfterContactMeButtonClick;

    @FindBy (how = How.XPATH,using = "//a[@href='#/en/progress']")
    WebElement myOrdersButtonOnMenu;

    // Added after 20.02.2018
    @FindBy (how = How.XPATH,using = "//span[@class='d-md-down-none']")
    WebElement rightTopMenu;

    @FindBy (how = How.XPATH,using = "//span[contains(text(), 'Logout')]")
    WebElement logoutButton;

    @FindBy (how = How.XPATH,using = "//div[@class='card p-4']")
    WebElement loginComponent;

    @FindBy (how = How.XPATH,using = "//div[@role='alert']")
    WebElement errorLoginStatement;






    //Methods

    public void closeCoockie (){
        closeCoockieButton.click();
    }
    public void openHomePage(){
        driver.get(homepage);
        pause(1);
    }

    public void logToMagnolia () {
        userNameInputSpaceMagnolia.sendKeys(magnolialogin);
        passwordInputSpaceMagnolia.sendKeys(magnolialogin);
        loginButtonMagnolia.click();

    }
    public void loginToCustomerArea() {
        //waitFormVisibleWebElement(loginButtonCustomerArea);
        loginButtonCustomerArea.click();
        pause(10);
        driver.manage().window().maximize();
    }
    public void checkIsSectionClosed () {
        waitFormVisibleWebElement(titleFromSecondSection);
        pasttitle = titleFromSecondSection.getText();
        closeFirstSectionButton.click();
        pause(2);
        presenttitle = titleFromFirstSection.getText();
        Assert.assertNotEquals(pasttitle,presenttitle, "If title are not closed it means, close section button is not workign");
    }

    public void closeFirstSection () {
        new Actions(driver).click(closeFirstSectionButton).build().perform();
    } // Method for close first section - I used it for check is Actions working corectly

    public void checkIsLanguageSetOnEnglish() {
        Assert.assertTrue(englishLanguageOnLoginpage.isEnabled());
    }

    public void moveFirstSectionToSecondSection() {
        Actions action = new Actions(driver);
        String title1 = titleFromFirstSection.getText();
        System.out.println(title1);
        action.dragAndDropBy(buttonForMoveFirstSection, 100,0).perform();
        String title2 = titleFromSecondSection.getText();
        System.out.println(titleFromSecondSection.getText());
        pause(1);
        Assert.assertEquals(title1,title2);
    }
    public void impersonateAsTBSCG () {
        impersonateDropdown.click();
        pause(1);
        tbscgImpersonateDropdown.click();
        pause(1);
    }

    // Dodane po 23.01.2018

    public void checkIsMyOrderContactMeSend () {
        contactMeButton.click();
        pause(5);
        System.out.println(statementAfterContactMeButtonClick.getText());
        Assert.assertEquals(statementAfterContactMeButtonClick.getText(),sendMessage);
        System.out.println(statementAfterContactMeButtonClick.getText());
    }
    public void setLanguageOnFrenchandCheckIsChanged() {
        changeLanguageButtonOnLoginpage.click();
        pause(2);
        frenchLanguageOnPopupLoginPage.click();
        pause(3);
        changeLanguageButtonOnLoginpage.click();
        pause(2);
        Assert.assertTrue(frenchLanguageDisabled.isDisplayed());
        System.out.println(frenchLanguageDisabled.isDisplayed());

    }

    public void openLocalHost () {
        driver.get(localhost);
        pause(1);
    }

    public void logOut () {
        rightTopMenu.click();
        pause(1);
        logoutButton.click();

    }
    public void checkIsUserLoggedOut () {
        Assert.assertTrue(loginComponent.isDisplayed());

    }

    public void checkIsIncorrectPasswqordstatement() {
        pause(1);
        Assert.assertTrue(errorLoginStatement.isDisplayed());

    }
    public void incorrectLogin() {
        usernameInputSpaceCustomerArea.sendKeys(sendMessage);
        passwordInputSpaceCustomerArea.sendKeys(sendMessage);
        loginButtonCustomerArea.click();

    }
    public void clearLoginAndPasswordInputspace() {
        usernameInputSpaceCustomerArea.clear();
        passwordInputSpaceCustomerArea.clear();
    }


    // Dodane po 28.02






}
