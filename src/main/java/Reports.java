import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

/**
 * Created by kjaraszek on 1/9/2018.
 */
public class Reports extends Dashboard {
    public Reports(WebDriver driver) {
        super(driver);
    }
    //xpaths
    @FindBy (how = How.XPATH, using = "//input[@placeholder='Date from']")
    private WebElement dateFrom;

    @FindBy (how = How.XPATH, using = "//input[@placeholder='Date to']")
    private WebElement dateTo;

    @FindBy (how = How.XPATH, using = "//div[@class='w-100 vdp-datepicker__calendar']")
    private WebElement datePicker;


    @FindAll({@FindBy(xpath="//div[@class='card-body bg-default px-0 px-xl-2']")})
    private List<WebElement> chartsBodyList;

    @FindAll({@FindBy(xpath="//canvas")})
    private List<WebElement> chartsBodyListOnLocalhost;

    @FindBy (how = How.XPATH,using = "//div[@class='card']")
    private WebElement chartBody; // ten xpath występuje zawsze, nawet jeśli wykres się nie wyświetla

    // Added after 28.02.2018

    @FindBy (how = How.XPATH,using = "//div[@class='nav-link nav-dropdown-toggle']")
    private  WebElement openReportsMinilist;

    @FindBy (how = How.XPATH,using = "//a[@href='#/fr/charts?granularity=WEEK']")
    private WebElement weeklyReports;








    // methods

    public void opendatepickerFrom ()  {
        chartsButtonOnMenu.click();
        pause(2);
        dateFrom.click();
    }
    public void checkisDatepickerOpen() {
        waitForClickableWebElement(datePicker);
        datePicker.isDisplayed();
    }
    public void checkIsChartsDisplaying() {
        chartsButtonOnMenu.click();
        pause(40);
        Integer length = chartsBodyList.size();
        System.out.println(length);
        Assert.assertTrue(length==10, " One of chars is not displaying.");
    }
    // Added after 20.02.2018
    public void checkIsChartsDisplayingOnLocalhost() {
        chartsButtonOnMenu.click();
        pause(40);
        Integer length = chartsBodyListOnLocalhost.size();
        System.out.println(length);
        Assert.assertTrue(length==9, " One of chars is not displaying.");
    }




}


