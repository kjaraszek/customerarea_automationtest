import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

/**
 * Created by kjaraszek on 3/9/2018.
 */
// Class created after 28.02
public class MyInvoices extends Dashboard {
    public MyInvoices(WebDriver driver) {
        super(driver);
    }

    // xpaths
    @FindBy(how = How.XPATH, using = "//a[@href='#/en/invoices']")
    private WebElement invoicesOpenbutton;

    @FindBy (how = How.XPATH,using = "//button[@class='btn btn-info dropdown-toggle']")
    private WebElement selectColumnsButton;

    @FindAll({@FindBy(xpath="//th")})
    private List<WebElement> topColumnsList;

    @FindAll({@FindBy(xpath="//label[@class='d-block my-2 mx-3 custom-control custom-checkbox']")})
    private List<WebElement> selectColumnCheckBoxList;


    //methods

    public void openMyInvoices() {
        invoicesOpenbutton.click();
        pause(1);
    }
    public void openSelectColumn (){
        selectColumnsButton.click();
    }
    public void clickAllColumnsCheckBox() {
        for ( int i=0; i<selectColumnCheckBoxList.size(); i++){
            selectColumnCheckBoxList.get(i).click();
        }
    }

}
